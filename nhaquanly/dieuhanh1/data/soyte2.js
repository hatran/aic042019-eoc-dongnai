﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart('container1', {
        chart: {
            type: 'column',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([100, 85, 78, 138, 135, 82]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0, 0, 0, 0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 1700);
            //    }
            //}
        },
        title: {
            text: 'Biểu đồ chương trình phòng chống Lao',
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Lao phổi M (+) mới', 'Lao phổi M (+) tái trị', 'Lao phổi M (-) mới', 'Lao ngoài phổi ', 'Lao kháng thuốc', 'Tổng số tiêu bản đàm 2'],
            labels: {
                autoRotation: 0,

            }
        },
        yAxis: {
            max:140,
            title: {
                text: '%'
            }
        },
        //colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "Lao phổi M (+) mới",
                    "y": 100,
                },
                {
                    "name": "Lao phổi M (+) tái trị",
                    "y": 85,
                },
                {
                    "name": "Lao phổi M (-) mới",
                    "y": 78,
                },
                {
                    "name": "Lao ngoài phổi ",
                    "y": 138,
                },
                {
                    "name": "Lao kháng thuốc",
                    "y": 135,
                },
                {
                    "name": "Tổng số tiêu bản đàm 2",
                    "y": 82,
                }
              ]
          }
        ],
        
    });
    
});