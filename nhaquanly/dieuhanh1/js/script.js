﻿var lat = 10.900494;
var lon = 106.984275;
var map = new L.Map('map', {
    zoom: 20,
    minZoom: 4,
});

// create a new tile layer
var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl, {
        attribution: '', //'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 16
    });

// add the layer to the map
map.addLayer(layer);

var cuuthuong = getParameterByName('ct');
var cuuhoa = getParameterByName('ch');
var chihuy = getParameterByName('chh');

// var lstPolice = [{
//     name: 'Công an phường Đông Ngạc',
//     lat: '21.0863999',
//     lon: '105.78120060000003',
//     address: 'Ngõ 35 Đông Ngạc, Đông Ngạc, Bắc Từ Liêm, Hà Nội'
// },{
//     name: 'Công an phường Thụy Phương',
//     lat: '21.0925584',
//     lon: '105.77541229999997',
//     address: 'Thụy Phương, Thuỵ Phương, Từ Liêm, Hà Nội'
// },{
//     name: 'Công an phường Phú Thượng',
//     lat: '21.0883183',
//     lon: '105.80889520000005',
//     address:'379 Lạc Long Quân, Phú Thượng, Tây Hồ, Hà Nội'
// }];

/*var lstFirefighter = [{
    name: 'PCCC số 3 huyện Tân Thành',
    lat: '10.0426',
    lon: '105.7783',
    address: 'TT. Phú Mỹ, Tân Thành, Bà Rịa - Vũng Tàu, Việt Nam'
}]*/

var lstFirefighter = [{
    name: 'Phòng cảnh sát PCCC Trảng Bom',
    lat: '10.95241',
    lon: '106.99738',
    address: 'TT. Tràng Bỏm, Trảng Bom, Đồng Nai, Việt Nam'
}]

var lstHospital = [{
    name: 'Bệnh viện Đa khoa Quốc tế Long Bình',
    lat: '10.951524',
    lon: '106.993853',
    address: 'TT. Tràng Bỏm, Trảng Bom, Đồng Nai, Việt Nam'
}]


var xethang1 = [
    [lstFirefighter[0].lat, lstFirefighter[0].lon],
    [10.94726,106.98090],
    [lat, lon]
];

var xethang2 = [
    [10.95716,106.96216],
    [10.95269,106.97168],
    [10.94726,106.98090],
    [lat, lon]
];

/*
var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.59720,107.05835],
    [10.59394,107.05825],
    [10.59402,107.05460],
    [10.58932,107.05517],
    [10.58824,107.05512],
    [0.58877,107.04084],
    [lat, lon]
];
*/
var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.94726,106.98090],
    [lat, lon]
];
// var xecuuthuong2 = [[lstHospital[0].lat + 0.01, lstHospital[0].lon - 0.02],[lat, lon]];


var londonBrusselFrankfurtAmsterdamLondon = [
    [51.507222, -0.1275],
    [50.85, 4.35],
    [50.116667, 8.683333],
    [52.366667, 4.9],
    [51.507222, -0.1275]
];

var barcelonePerpignanPauBordeauxMarseilleMonaco = [
    [41.385064, 2.173403],
    [42.698611, 2.895556],
    [43.3017, -0.3686],
    [44.837912, -0.579541],
    [43.296346, 5.369889],
    [43.738418, 7.424616]
];


//map.fitBounds(mymap);

map.setView([10.92440,106.98232], 13);

var FireIcon = L.icon({
    iconUrl: 'images/fire-2-32_2.gif',
    iconSize: [36, 36], // size of the icon
});

var CCIcon = L.icon({
    iconUrl: 'images/Ol_icon_blue_example.png',
    iconSize: [32, 32], // size of the icon
});

var CTIcon = L.icon({
    iconUrl: 'images/Ol_icon_red_example.png',
    iconSize: [32, 32], // size of the icon
});

// var CHIcon = L.icon({
//     iconUrl: 'images/jeep.png',
//     iconSize: [32, 32], // size of the icon
// });

var marker = L.marker([lat, lon], { icon: FireIcon }).bindPopup('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Khu Công Nghiệp Giang Điền</span></p>').addTo(map);
marker.openPopup();
// marker.on('mouseover', function(e) {
//         //open popup;
//         var popup = L.popup()
//             .setLatLng(e.latlng)
//             .setContent('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Khu Công Nghiệp Phúc Khánh</span></p>')
//             .openOn(map);
//     });

if (cuuhoa == 1) {
    //========================================================================
    var marker1 = L.Marker.movingMarker(xethang1,
        [10000, 20000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

    marker1.loops = 0;
    marker1.bindPopup();
    marker1.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe thang 1 thuộc: ' + lstFirefighter[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường')
            .setContent('Xe thang 1 <br> Còn <strong>8</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    marker1.start();

    var marker2 = L.Marker.movingMarker(xethang2,
        [20000, 10000, 20000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

    marker2.loops = 0;
    marker2.bindPopup();
    marker2.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            .setContent('Xe thang 2 <br> Còn <strong>10</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    marker2.start();
}

if (cuuthuong == 1) {
    var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
        [20000, 20000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

    markerCT1.loops = 0;
    markerCT1.bindPopup();
    markerCT1.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe cứu thương 1 thuộc: ' + lstHospital[2].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
            .setContent('Xe cứu thương 1 <br> Còn <strong>10</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    markerCT1.start();



}

var route1 = new L.Routing.control({
    waypoints: [
        L.latLng(lstFirefighter[0].lat, lstFirefighter[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "red", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route2 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[0].lat, lstHospital[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "blue", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}